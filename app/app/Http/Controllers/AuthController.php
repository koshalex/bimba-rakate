<?php

namespace App\Http\Controllers;

use App\Achievement;
use App\Http\Controllers\Traits\Statusable;
use App\Http\Requests\User\UserRequest;
use App\Leaderboard;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use Statusable;

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserRequest $request)
    {
        $user = User::where('username', $request->get('username'))->first();

        if (!$user) {
            $user = $this->register($request->get('username'));//TODO: ADD SAME NAME ATTEMPT
//            return response()->json(['error' => 'invalid_credentials'], 401);
        }
        $userToken = JWTAuth::fromUser($user);

        return response()->json(['status' => 'success'], 200)
            ->header('Authorization', $userToken);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Logged out'
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        $user = Auth::user();

        return response()->json([
            'status'    => 'success',
            'data'      => $user
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    /**
     * @param $username
     * @return array
     */
    protected function register($username)
    {
        $user = User::create(['username' => $username]);
        $this->addRelations($user);
        return $user;
    }

    /**
     * @param User $user
     */
    private function addRelations(User $user)
    {
        $user->achievements()->save(factory(Achievement::class)->make([
            'wins_amount'       => 0,
            'wins_in_row'       => 0,
            'rockets_launched'  => 0,
            'total_login_days'  => 1,
            'day_matches'       => 0
        ]));
        $user->leaderboards()->save(factory(Leaderboard::class)->make([
            'position' => null
        ]));
    }

    /**
     * @return mixed
     */
    private function guard()
    {
        return Auth::guard();
    }
}
