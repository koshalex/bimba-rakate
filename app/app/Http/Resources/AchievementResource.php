<?php

namespace App\Http\Resources;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Http\Resources\Json\JsonResource;

class AchievementResource extends JsonResource
{
    use Authorizable;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();

        return [
            'id'                => $this->id,
            'user_id'           => $this->user_id,
            'wins_amount'       => $this->wins_amount,
            'wins_in_row'       => $this->wins_in_row,
            'rockets_launched'  => $this->rockets_launched,
            'total_login_days'  => $this->total_login_days,
            'day_matches'       => $this->day_matches
        ];
    }
}