<?php

namespace App\Http\Resources;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Http\Resources\Json\JsonResource;

class LeaderboardResource extends JsonResource
{
    use Authorizable;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();

        return [
            'id'        => $this->id,
            'username'  => $this->username,
            'user_id'   => $this->user_id,
            'position'  => $this->position,
            'user'      => UserResource::make($this->whenLoaded('users')),
        ];
    }
}
