<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() {
        return User::with('achievements', 'leaderboards')->get();
    }
}