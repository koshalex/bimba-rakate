<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Leaderboard extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'leaderboards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'position'];

    /**
     * @return mixed
     */
    public function getUsernameAttribute() {
        return $this->user->username;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
