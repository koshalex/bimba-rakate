<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Eloquent implements Authenticatable, JWTSubject
{
    use AuthenticableTrait;
    use Notifiable;

    public const UNRANKED = "Unranked";

    protected $connection = 'mongodb';
    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['remember_token'];

    protected $appends = ['position'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return mixed
     */
    public function getPositionAttribute() {
        return $this->leaderboards()->first()
            ? $this->leaderboards->position
            : self::UNRANKED;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function achievements() {
        return $this->hasOne(Achievement::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function leaderboards() {
        return $this->hasOne(Leaderboard::class);
    }

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
