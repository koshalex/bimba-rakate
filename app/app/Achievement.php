<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Achievement extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'achievements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'wins_amount', 'wins_in_row', 'rockets_launched',
        'total_login_days', 'day_matches'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
