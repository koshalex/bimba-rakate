import VueRouter from 'vue-router'

import Home from './pages/Home'
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true
        }
    }
];

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes: router
});

export default router