<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Achievement;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Achievement::class, function (Faker $faker) {
    return [
//        'user_id'           => factory(User::class)->create()->id,
        'wins_amount'       => $faker->randomDigit,
        'wins_in_row'       => $faker->randomDigit,
        'rockets_launched'  => $faker->randomDigit,
        'total_login_days'  => $faker->randomDigit,
        'day_matches'       => $faker->randomDigit
    ];
});
