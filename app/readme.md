## Install

```
docker-compose up
```
copy .env file:
```
cp .env.example .env #Linux

copy .env.example .env #Windows
```
then add to your hosts file:

```
127.0.0.1 demo.local #Linux

10.0.75.2 demo.local #Windows
```
enter to container: 
```
docker exec -ti api_app bash |or| docker exec -ti todoko5ch_app /bin/bash |or| docker exec -ti api_app sh
```

install dependencies with composer:
```
composer install 
```
generate application key:
```
php artisan key:generate
```
install node dependencies from project folder
## DONE!